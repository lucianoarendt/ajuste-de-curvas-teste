import matplotlib.pyplot as plt
import numpy as np
   
n = 20

sxy = 0
sx = 0
sx2 = 0
sy = 0

xs = np.arange(0,10,0.5)
ys = []

for i in range(n):
    yi = np.random.randn() + xs[i]
    sx += xs[i]
    sy += yi
    sx2 += xs[i]*xs[i]
    sxy += xs[i]*yi
    ys.append(yi)
     
a1 = (n*sxy - sx*sy)/(n*sx2 - sx*sx)
a0 = (sy - a1*sx)/n

xr = np.arange(0, 10, 0.1)
yr = a1*xr + a0

plt.plot(xr, yr)
plt.plot(xs, ys, 'ro')

plt.show()