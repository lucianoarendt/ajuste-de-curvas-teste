import matplotlib.pyplot as plt
import numpy as np
   
n = int(input("Digite quantos dados serao inseridos: "))

sxy = 0
sx = 0
sx2 = 0
sy = 0

xs = []
ys = []

for i in range(n):
    xi = float(input())
    yi = float(input())
    sx += xi
    sy += yi
    sx2 += xi*xi
    sxy += xi*yi
    xs.append(xi)
    ys.append(yi)
     
a1 = (n*sxy - sx*sy)/(n*sx2 - sx*sx)
a0 = (sy - a1*sx)/n

xr = np.arange(0, 10, 0.1)
yr = a1*xr + a0

plt.plot(xr, yr)
plt.plot(xs, ys, 'ro')

plt.show()
